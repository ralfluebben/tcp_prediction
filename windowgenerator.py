
import numpy as np
from copy import deepcopy
import gc

import tensorflow as tf

from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.colors import LogNorm

class WindowGenerator():
  def __init__(self, input_start, input_width, label_width, shift,
               train_df, val_df, test_df, label_columns, feature_columns, scaling):

    # print("TAGS " + str(train_df.attrs.keys()))
    # if "tag" in train_df.attrs.keys():
    #   self.train_df_slice_tags = train_df.attrs["tag"]
    #   self.val_df_slice_tags = val_df.attrs["tag"]
    #   self.test_df_slice_tags = test_df.attrs["tag"]
    #   print(test_df.attrs["tag"].shape)
    # else:
    self.train_df_slice_tags = None
    self.val_df_slice_tags = None
    self.test_df_slice_tags = None

    self.train_mean = np.zeros(train_df.shape[2])
    self.train_std = np.ones(train_df.shape[2])

    if scaling == True:
      self.train_mean = train_df.min(axis=(0,1))
      self.train_std = train_df.max(axis=(0,1))-train_df.min(axis=(0,1))

    print(self.train_mean)
    print(self.train_std)

    # Store the raw data.
    self.train_df = (train_df - self.train_mean) / self.train_std
    del train_df
    self.val_df = (val_df - self.train_mean) / self.train_std
    del val_df
    self.test_df = (test_df - self.train_mean) / self.train_std
    del test_df
    
    assert not np.any(np.isnan(self.train_df))
    assert not np.any(np.isnan(self.val_df))
    assert not np.any(np.isnan(self.test_df))

    self.train_df_slice = None
    self.val_df_slice = None
    self.test_df_slice = None
    
    # Work out the label column indices.
    self.label_columns = label_columns
    if label_columns is not None:
      self.label_columns_indices = {name: i for i, name in enumerate(label_columns)}
    self.column_indices = {name: i for i, name in enumerate(self.train_df.coords["values"].values)}

    # set the normalize value used for labels
    self.label_norm = self.train_std
    if self.label_columns is not None:
      print (self.label_columns)
      self.label_norm = np.stack([self.label_norm[self.column_indices[name]] for name in self.label_columns], axis=-1)

    self.label_mean = self.train_mean
    if self.label_columns is not None:
      print (self.label_columns)
      self.label_mean = np.stack([self.label_mean[self.column_indices[name]] for name in self.label_columns], axis=-1)

    # Work out the label column indices.
    self.feature_columns = feature_columns
    if feature_columns is not None:
      self.feature_columns_indices = {name: i for i, name in enumerate(feature_columns)}
    self.feature_indices = {name: i for i, name in enumerate(self.train_df.coords["values"].values)}

    # Work out the window parameters.
    self.input_start = input_start
    self.input_width = input_width
    self.label_width = label_width
    self.shift = shift

    self.total_window_size = input_start + input_width + shift + label_width

    self.input_slice = slice(input_start, input_start+input_width)
    self.input_indices = np.arange(self.total_window_size)[self.input_slice]

    self.label_start = input_start + input_width + shift
    self.labels_slice = slice(self.label_start, self.label_start + self.label_width)
    self.label_indices = np.arange(self.total_window_size)[self.labels_slice]

    # only relevant if we know future interarrivals
    self.input_slice_future = slice(input_start, self.label_start + self.label_width)
    self.input_future_indices = np.arange(self.total_window_size)[self.input_slice_future]

    self.ts_test = self.make_ts(self.test_df)

    # create slices, this causes deletion of complete time series and only keeps slices
    x,y = self.test
    x,y = self.val
    x,y = self.train
    gc.collect()

  def __repr__(self):
    return '\n'.join([
        f'Input indices: {self.input_indices}',
        f'Label indices: {self.label_indices}',
        f'Label column name(s): {self.label_columns}'])

  def get_label_norm(self):
    return self.label_norm

  def get_label_mean(self):
    return self.label_mean

  def make_dataset(self, data):
    
    labels = deepcopy(data.values[:, self.labels_slice,:])
    
    if self.label_columns is not None:
      labels = np.stack([labels[:, :, self.column_indices[name]] for name in self.label_columns], axis=-1)
    labels=np.reshape(labels, (-1,labels.shape[1]))


    inputs = deepcopy(data.values[:, self.input_slice, :])
    if self.feature_columns is not None:
      inputs = np.stack([inputs[:, :, self.feature_indices[name]] for name in self.feature_columns], axis=-1)
    #print(inputs)
    return  inputs, labels

  def make_ts(self, data):
    ts = np.stack([data.values[:, :, self.column_indices["ts"]]], axis=-1)
    ts=np.reshape(ts, (-1,ts.shape[1]))
    return ts  

  @property
  def train(self):
    if self.train_df_slice is None:
      self.train_df_slice = self.make_dataset(self.train_df)
      del self.train_df
      self.train_df = None
      gc.collect()
    return self.train_df_slice

  @property
  def val(self):
    if self.val_df_slice is None:
      self.val_df_slice = self.make_dataset(self.val_df)
      del self.val_df
      self.val_df = None
      gc.collect()
    return self.val_df_slice

  @property
  def test(self):
    if self.test_df_slice is None:
      self.test_df_slice = self.make_dataset(self.test_df)
      del self.test_df
      self.test_df = None
      gc.collect()
    return self.test_df_slice

  def example(self):
    """Get and cache an example batch of `inputs, labels` for plotting."""
    result = getattr(self, '_example', None)
    if result is None:
      # No example batch was found, so get one from the `.train` dataset
      result = next(iter(self.val))
      # And cache it for next time
      self._example = result
    return result

  def plot_features(self, max_subplots=25, run_dir="."):
    inputs, labels = self.test
    for fea in self.feature_columns:
      i = self.feature_columns_indices[fea]
      plt.figure(figsize=(12, int(3*max_subplots)))
      for n in range(max_subplots):
          ax=plt.subplot(max_subplots, 1, n+1)
          y=(inputs[n,:,i] * self.train_std[i].values) + self.train_mean[i].values
          plt.plot(y, label=fea, marker='.', zorder=-10)
      plt.xlabel('packet [#]')
      plt.tight_layout()
      fn=fea+".pdf"
      plt.savefig(fn)
      plt.close()


  def plot(self, model=None, plot_col='rate', max_subplots=25, run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    feature_plot_col = plot_col
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[feature_plot_col]
    
    max_n = min(max_subplots, len(inputs))
    plt.figure(figsize=(12, int(3*max_n)))
    for n in range(max_n):
      ax=plt.subplot(max_n, 1, n+1)
      plt.ylabel(f'{plot_col} [s]')
 
      x = (inputs[n, :, feature_col_index] * self.train_std[feature_col_index].values) + self.train_mean[feature_col_index].values
      plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', marker='.', zorder=-10)

      if self.label_columns:
        label_col_index = self.label_columns_indices.get(plot_col, None)
      else:
        label_col_index = plot_col_index

      if label_col_index is None:
        continue
     
      l = "label" # add measurement tag if defined, e.g. mm1_75
      if self.test_df_slice_tags is not None:
          print(self.test_df_slice_tags[n])
          l = l + " " + str(self.test_df_slice_tags[n][0])
      else:
          print("No tags")

      y = (labels[n, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
      plt.scatter(self.label_indices, y,
                  edgecolors='k', label=l, c='#2ca02c', s=32)
      #plt.ylim(0,140)
      if model is not None:
        predictions = (model(inputs[0:max_subplots,:,:])*self.get_label_norm())+self.train_mean[plot_col_index].values
        plt.scatter(self.label_indices, predictions[n, :],
                    marker='X', edgecolors='k', label='predicted quantile',
                    c='#ff7f0e', s=32)
        mse = tf.keras.losses.MeanSquaredError()
        mae = tf.keras.losses.MeanAbsoluteError()
        mape = tf.keras.losses.MeanAbsolutePercentageError()
        y = (labels[:, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
        #predictions = (model(inputs[:,:,:])*self.get_label_norm())+self.train_mean[plot_col_index].values
        #print("MSE", mse(y, predictions).numpy(), "MAE", mae(y, predictions).numpy() , "MAPE", mape(y, predictions).numpy())
        ax2 = ax.twiny()
        ts_col_index = self.feature_columns_indices["ts"]
        x2 = np.cumsum(self.ts_test[n,:]*self.train_std[ts_col_index].values + self.train_mean[ts_col_index].values)
        ax1Xs = ax.get_xticks()
        #ax2.set_xticks(ax1Xs)
        ax2.set_xbound(ax.get_xbound())
        ax2Xs_labels = []
        ax2Xs_ticks = []
        #print(ax1Xs)
        #print(x2)
        for i in range(0,len(ax1Xs)):
          j = int(ax1Xs[i])
          if j>=0 and j<len(x2):
            ax2Xs_labels.append(round(x2[j],2))
            ax2Xs_ticks.append(ax1Xs[i])
        ax2.set_xticks(ax2Xs_ticks)
        ax2.set_xticklabels(ax2Xs_labels)

      #if n == 0:
      delays = x[-1];
      rate = y*8;
      bdp = delays*rate/12e3;
      p_rate =  predictions[n]*8;
      p_bdp = delays*p_rate/12e3;
      print(bdp, p_bdp)
      #plt.yscale('log')
      plt.legend(loc=2)
    
    plt.xlabel('packet [#]')
    plt.tight_layout()
    if fig_extension != "":
      tag = 'distance_' + fig_extension + ".pdf"
    else:
      tag='distance.pdf'

    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def plot_single(self, model=None, plot_col='delay', max_subplots=10, run_dir=".", fig_extension=""):
    inputs, labels = self.test
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    max_n = min(max_subplots, len(inputs))
    for n in range(max_n):
      plt.figure()
      plt.ylabel(f'{plot_col} [s]')
 
      x = (inputs[n, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
      plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', marker='.', zorder=-10)

      if self.label_columns:
        label_col_index = self.label_columns_indices.get(plot_col, None)
      else:
        label_col_index = plot_col_index

      if label_col_index is None:
        continue
     
      l = "label" # add measurement tag if defined, e.g. mm1_75
      if self.test_df_slice_tags is not None:
          print(self.test_df_slice_tags[n])
          l = l + " " + str(self.test_df_slice_tags[n][0])
      else:
          print("No tags")

      y = (labels[n, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
      plt.scatter(self.label_indices, y,
                  edgecolors='k', label=l, c='#2ca02c', s=32)
      if model is not None:
        predictions = (model(inputs[0:max_subplots,:,:])*self.get_label_norm())+self.train_mean[plot_col_index].values
        plt.scatter(self.label_indices, predictions[n, :],
                    marker='X', edgecolors='k', label='predicted quantile',
                    c='#ff7f0e', s=32)
      #if n == 0:
      plt.legend()
      #plt.ylim(0, 140)
      plt.yscale('log')
      plt.xlabel('packet [#]')
      plt.tight_layout()
      if fig_extension != "":
        tag = 'distance_' + fig_extension + "_" + str(n) + ".pdf"
      else:
        tag='distance.pdf'

      fn=run_dir + '/' + tag
      print ("Save: " + fn)
      plt.savefig(fn)

  def plot_mean(self, model=None, plot_col='rate', run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    plt.figure(figsize=(12, 3))
   
    plt.ylabel(f'{plot_col} [s]')

    x = np.quantile(inputs[:, :, feature_col_index], 0.95, axis=0) * self.train_std[plot_col_index].values + self.train_mean[plot_col_index].values
    print(x.shape)
    plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', marker='.', zorder=-10)

    if self.label_columns:
      label_col_index = self.label_columns_indices.get(plot_col, None)
    else:
      label_col_index = plot_col_index

    y = np.quantile(labels[:, :],0.95, axis=0)*self.get_label_norm()+self.train_mean[plot_col_index].values
    plt.scatter(self.label_indices, y,
                edgecolors='k', label='empirical quantile', c='#2ca02c', s=32)
    if model is not None:
      predictions = np.mean(model.predict(inputs),axis=0)*self.get_label_norm()+self.train_mean[plot_col_index].values
      plt.scatter(self.label_indices, predictions,
                  marker='X', edgecolors='k', label='mean predicted quantile',
                  c='#ff7f0e', s=32)
    
    plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    if fig_extension != "":
      tag = 'mean_distance_' + fig_extension + ".pdf"
    else:
      tag='mean_distance.pdf'
    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def plot_ts_delay(self, plot_col='rate', run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    plt.figure(figsize=(12, 3))
    plt.ylabel(f'{plot_col} [s]')
    
    x = (inputs[0, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
    plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs', marker='.', zorder=-10)

    y = (labels[0, :]*self.get_label_norm())+self.train_mean[plot_col_index].values
    plt.scatter(self.label_indices, y,edgecolors='k', label='features', c='#2ca02c', s=32)
    ax = plt.gca()
    ylim_1,ylim=ax.get_ylim()
    ax.set_ylim([ylim_1, 1.3*ylim])

    dx=(self.input_indices[-1]-self.input_indices[0])
    plt.arrow(self.input_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.input_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2,length_includes_head=True)

    x_text=self.input_indices[0]+(self.input_indices[-1]-self.input_indices[0])/2
    ax.annotate("inputs 1",
            xy=(self.input_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )

    dx=(self.label_indices[-1]-self.label_indices[0])
    plt.arrow(self.label_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.label_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
            
    x_text=self.label_indices[0]+(self.label_indices[-1]-self.label_indices[0])/2
    ax.annotate("labels",
            xy=(self.label_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )
     
    if self.label_columns:
      label_col_index = self.label_columns_indices.get(plot_col, None)
    else:
      label_col_index = plot_col_index
        
    plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    tag = 'rate' + fig_extension + ".pdf"
    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def plot_iat_delay(self, plot_col='rate', run_dir=".", fig_extension=""):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]
    feature_col_index = self.feature_columns_indices[plot_col]
    plt.figure(figsize=(12, 3))
    plt.ylabel(f'{plot_col} [s]')
    
    # first iats
    x = (inputs[0, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
    plt.plot(self.input_indices, x[0:len(self.input_indices)], label='inputs 2', marker='.', zorder=-10)

    # future iats
    x = (inputs[0, :, feature_col_index] * self.train_std[plot_col_index].values) + self.train_mean[plot_col_index].values
    plt.plot(self.input_future_indices, x[0:len(self.input_future_indices)], label='inputs 3', marker='.', zorder=-10)
    
    ax = plt.gca()
    ylim_1,ylim=ax.get_ylim()
    ax.set_ylim([ylim_1, 1.3*ylim])

    dx=(self.input_indices[-1]-self.input_indices[0])
    plt.arrow(self.input_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.input_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2,length_includes_head=True)

    x_text=self.input_indices[0]+(self.input_indices[-1]-self.input_indices[0])/2
    ax.annotate("inputs 2",
            xy=(self.input_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )

    dx=(self.label_indices[-1]-self.label_indices[0])
    plt.arrow(self.label_indices[0],1.1*ylim,dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
    plt.arrow(self.label_indices[-1],1.1*ylim,-dx,0, width=0.01, head_width=0.1, head_length=2, length_includes_head=True)
            
    x_text=self.label_indices[0]+(self.label_indices[-1]-self.label_indices[0])/2
    ax.annotate("inputs 3",
            xy=(self.label_indices[0], 1.15*ylim), xycoords='data',
            xytext=(x_text, 1.15*ylim), textcoords='data',
            horizontalalignment="center"
    )

    if self.label_columns:
      label_col_index = self.label_columns_indices.get(plot_col, None)
    else:
      label_col_index = plot_col_index
        
    plt.legend()

    plt.xlabel('packet [#]')
    plt.tight_layout()
    tag = 'iat_ts_' + fig_extension + ".pdf"
    fn=run_dir + '/' + tag
    print ("Save: " + fn)
    plt.savefig(fn)

  def stats_per_tag(self, model, plot_col='rate'):
    inputs, labels = self.test
    plot_col_index = self.column_indices[plot_col]
    labels = labels*self.get_label_norm()+self.train_mean[plot_col_index].values
    predictions = (model.predict(inputs)*self.get_label_norm())+self.train_mean[plot_col_index].values
    
    utags = []
    for t in self.test_df_slice_tags:
      if t not in utags:
        utags.append(t[0])
    d={}
    q={}
    dc={}
    qc={}
    for t in utags:
      d[t]=0
      q[t]=0
      dc[t]=0
      qc[t]=0
    n=0

    for tag in self.test_df_slice_tags:
      #print(tag)
      d[tag[0]]+=(np.sum(predictions[n, :] -  labels[n, :]))
      dc[tag[0]]+=predictions.shape[1]
      q[tag[0]]+=(np.sum(predictions[n, :] <  labels[n, :]))
      qc[tag[0]]+=predictions.shape[1]
      n=n+1

    msum=0
    sumc=0
    for i in q.keys():
      print(i)
      print(q[i]/qc[i])
      print(d[i]/dc[i])
      msum+=q[i]
      sumc+=qc[i]
    print(msum/sumc)
    import xarray as xr
    df=xr.DataArray([labels, predictions], dims=["true","pred", "values"])
    df.attrs["tag"]=self.test_df_slice_tags

  def stats(self, model, plot_col='rate'):
    inputs, labels = self.test
    plot_col_index = self.column_indices[plot_col]
    labels = labels*self.get_label_norm()+self.train_mean[plot_col_index].values
    predictions = (model.predict(inputs)*self.get_label_norm())+self.train_mean[plot_col_index].values

    d=0.0
    q=0.0
    dc=0.0
    qc=0.0
    n=0

    for n in range(0,predictions.shape[0]):
      d+=(np.sum(predictions[n, :] -  labels[n, :]))
      dc+=predictions.shape[1]
      q+=(np.sum(predictions[n, :] <  labels[n, :]))
      qc+=predictions.shape[1]

    print(q/qc)
    print(d/dc)

  def plot_hist(self, model):
    inputs, labels = self.test
    
    plot_col = feature_plot_col = 'rate'
    plot_col_index = self.column_indices[plot_col]
    
    plt.figure(figsize=(12, int(3*2)))

    ax=plt.subplot(2, 1, 1)
    plt.ylabel(f'{plot_col} [s]')
 
    #x = (inputs[n, :, feature_col_index] * self.train_std[feature_col_index].values) + self.train_mean
    y = (labels*self.get_label_norm())+self.train_mean[plot_col_index].values
    
    plt.figure(figsize=(12, 6))
    plt.subplot(2, 1, 1)
    plt.hist(y, bins='auto')
    plt.xlim(0,6e6)
    plt.subplot(2, 1, 2)
    
    predictions = (model.predict(inputs[:,:,:], batch_size=1024)*self.get_label_norm())+self.train_mean[plot_col_index].values
    plt.hist(predictions, bins='auto')
    plt.xlim(0,6e6)
    plt.savefig("hist.pdf")
  
  def plot_scatter(self, model, fig_extension="", plot_col='rate'):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]

    y = (labels*self.get_label_norm())+self.train_mean[plot_col_index].values
    predictions = (model.predict(inputs[:,:,:], batch_size=1024)*self.get_label_norm())+self.train_mean[plot_col_index].values

    #feature_plot_col = 'delay'
    #plot_col_index = self.column_indices[plot_col]
    #feature_col_index = self.feature_columns_indices[feature_plot_col]
    #delays = (inputs[:, -1, feature_col_index] * self.train_std[feature_col_index].values) + self.train_mean[feature_col_index].values

    #rate = y*8;
    #bdp = delays*rate.T/12e3;
    #p_rate =  predictions*8;
    #p_bdp = delays*p_rate.T/12e3;
    print(y.shape, predictions.shape)
    
    x=np.mean(y, axis=1).ravel()
    y=np.mean(predictions, axis=1)
    y=y.ravel()
    print(y.shape, predictions.shape)


    from matplotlib import cm
    from matplotlib.colors import LogNorm as LN
    xmin = 0 #x.min()
    xmax = 50 #x.max()
    ymin = 0 #y.min()
    ymax = 50 #np.min(45,y.max())

    fig, ax = plt.subplots(ncols=1) #, sharey=True, figsize=(7, 4))
    top = cm.get_cmap('Reds')
    y_clipped=y.copy()
    y_clipped[y>ymax]=ymax
    y_clipped[y<ymin]=ymin
    x_clipped=x.copy()
    x_clipped[x>xmax]=xmax
    x_clipped[x<xmin]=xmin
    hb = ax.hexbin(x_clipped, y_clipped, gridsize=(50,50), norm=LN(1,175, clip=True), cmap=top)
    print("+++++++++++++++++++++++++")
    print("max hexbin", np.max(hb.get_array()), np.sum(hb.get_array()))
    print("+++++++++++++++++++++++++")
    plt.plot([0,5000],[0,5000], 'k--')
    ax.set(xlim=(xmin, xmax), ylim=(ymin, ymax))
    #ax.set_title("With a log color scale")
    cb = fig.colorbar(hb, ax=ax)
    cb.set_label('counts')
    plt.xlabel('measured bandwidth [MBit/s]')
    plt.ylabel('predicted bandwidth [MBit/s]')
    fname="heatmap_"+fig_extension+".png"
    plt.savefig(fname)
    plt.close()

    plt.figure()
    plt.scatter(x,y, s=0.2)
    xmin = x.min()
    xmax = x.max()
    ymin = y.min()
    ymax = y.max()
    plt.xlim(xmin,xmax)
    plt.ylim(ymin, ymax)
    plt.plot([0,5000],[0,5000], 'k--')
    plt.xlabel('measured bandwidth [MBit/s]')
    plt.ylabel('predicted bandwidth [MBit/s]')
    fname="scatter_"+fig_extension+".png"
    plt.savefig(fname)

  def get_prediction(self, model, plot_col='rate'):
    inputs, labels = self.test
    
    plot_col_index = self.column_indices[plot_col]

    #y = (labels*self.get_label_norm())+self.train_mean[plot_col_index].values
    predictions = (model.predict(inputs[:,:,:], batch_size=1024)*self.get_label_norm())+self.train_mean[plot_col_index].values
   
    print(predictions.shape)
    predictions.ravel()
    return predictions.ravel()

  def inv_norm(self,x,plot_col='rate'):
    plot_col_index = self.column_indices[plot_col]
    return x*self.get_label_norm()+self.train_mean[plot_col_index].values


