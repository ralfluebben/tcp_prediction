import sys

local_vars = list(locals().items())
for var, obj in local_vars:
    type(obj)
    if (isinstance(obj, (xr.core.dataarray.DataArray))):
        print(var, obj.nbytes/(1024*1024*1024))


import h5netcdf
import xarray as xr
import numpy as np

df = xr.open_dataarray("data_small.nc")
print("Plot data")
max_n=25
for f in feature_columns:
    i = feature_columns.index(f)
    plt.figure(figsize=(12, int(3*max_n)))
    for n in range(max_n):
        ax=plt.subplot(max_n, 1, n+1)
        plt.plot(test_df[n,:,i], label=f, marker='.', zorder=-10)
    plt.xlabel('packet [#]')
    plt.tight_layout()
    fn=f+".pdf"
    plt.savefig(fn)
    plt.close()

python3 predict_from_tcpprobe.py -t Feedback -s 0 -w 20 -l 5 -r 0.0 -f 100 -b 0 -k 0 -u BayesianOptimization



my_func() {
    echo $1
  python3 predict_from_tcpprobe.py -t Feedback -s 0 -w 30 -l 1 -r 0.0 -f 100 -b 0 -k ${1} -n 50000 -u RandomSearch
}
export -f my_func
seq 8 28 | parallel -j 5 --ungroup my_func

for d in `ls -d FB_30_0_1_100_0.0_mape_*`
do
  r=`python3 ../get_best_trail.py $d | head -n 2 | tail -n 1`
  echo $r $d
done | sort