#python3 load_tcp_probe_data.py -t Dense -s 0 -w 30 -l 1 -r 0.0 -u RandomSearch &
#python3 load_tcp_probe_data.py -t Dense -s 0 -w 30 -l 1 -r 0.001 -u RandomSearch &
#python3 load_tcp_probe_data.py -t Dense -s 0 -w 30 -l 1 -r 0.0001 -u RandomSearch &
#python3 load_tcp_probe_data.py -t Dense -s 0 -w 30 -l 1 -r 0.00001 -u RandomSearch

#declare -a LR=("0.0" "0.001" "0.0001" "0.00001")
declare -a LR=("0.0")
#declare -a IW=("10" "30" "70")
#declare -a IW=("10" "20" "30")
#declare -a IW=("40")
declare -a OW=("1" "5" "10")
declare -a SH=("200" "100")
declare -a BS=("0")

I=$1

for L in "${LR[@]}"
do

#for I in "${IW[@]}"
#do

for O in "${OW[@]}"
do

for S in "${SH[@]}"
do

for B in "${BS[@]}"
do

python3 predict_from_tcpprobe.py -t Feedback -s 0 -w $I -l $O -r $L -f $S -b $B -n 50000 -k all -u RandomSearch

#done
done
done
done
done

for L in "${LR[@]}"
do
#for I in "${IW[@]}"
#do
for O in "${OW[@]}"
do
for S in "${SH[@]}"
do
for B in "${BS[@]}"
do
m=`python3 get_best_trail.py results/FB_${I}_0_${O}_${S}_${L}_mape_all_50000_bbr_bw/ | head -n 2 | tail -n 1 | cut -f 2 -d ' '`
echo $m
python3 predict_from_tcpprobe.py -t Feedback -s 0 -w $I -l $O -r $L -f $S -b $B -n 50000 -k all -u RandomSearch -m $m
done
done
done
done
