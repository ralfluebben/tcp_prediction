# load jsin trail file

import os
import os.path
import numpy as np
import json
import sys
filter_tag = sys.argv[1]
data_aggregated = None
results = []
for dirpath, dirnames, filenames in os.walk("."):
    for filename in [f for f in filenames if f.endswith("trial.json")]:
        fn = os.path.join(dirpath, filename)
        if filter_tag in fn:
            with open(fn) as json_file:
                data = json.load(json_file)
                #print(data)
                if (data['status'] == 'COMPLETED'):
                    try:
                        #if (data['metrics']['metrics']['quantile_metric']['observations'][0]['value'][0] <= 0.05):
                        data['path']=fn
                        results.append(data)
                    except:
                        print("Missing metric")
                        print(data['status'])
            
sorted_results=sorted(results, key=lambda k: k['metrics']['metrics']['val_loss']['observations'][0]['value'], reverse=False)

print("distance loss quantile tid")
for r in sorted_results:
    loss=r['metrics']['metrics']['val_loss']['observations'][0]['value']
    #dist=r['metrics']['metrics']['val_quantile_distance']['observations'][0]['value']
    #quan=r['metrics']['metrics']['val_quantile_metric']['observations'][0]['value']
    tid=r['trial_id']
    print(round(loss[0],5), tid)
    print(r['hyperparameters']['values'])
    #print(dist, loss, quan, tid)

print(sorted_results[0]['trial_id'])
print(sorted_results[0])