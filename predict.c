#include <stdio.h>
#include <tensorflow/c/c_api.h>
#include <time.h>
#include <stdlib.h>

void NoOpDeallocator(void* data, size_t a, void* b) {}

float predict(void * data, TF_Output* Input, TF_Tensor** InputValues, TF_Output* Output , TF_Tensor** OutputValues, TF_Session * Session)
{
    int ndims = 3;
    int64_t dims[] = {1, 30, 28};
    int ndata = sizeof(float) * 30 * 28;
    int NumInputs = 1;
    int NumOutputs = 1;

    TF_Status *Status = TF_NewStatus();

    printf("Create tensor\n");

    TF_Tensor *int_tensor1 = TF_NewTensor(TF_FLOAT, dims, ndims, data, ndata, &NoOpDeallocator, NULL);
    if (int_tensor1 != NULL)
    {
        printf("TF_NewTensor is OK for int_tensor1\n");
    }
    else
        printf("ERROR: Failed TF_NewTensor for int_tensor1\n");
    
    InputValues[0] = int_tensor1;

    clock_t start, end;
    double cpu_time_used;

    printf("Run session\n");
    start = clock();
    TF_SessionRun(Session, NULL,
                  Input, InputValues, NumInputs,
                  Output, OutputValues, NumOutputs,
                  NULL, 0, NULL, Status);
    end = clock();
    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("seesion run took %f seconds to execute \n", cpu_time_used);

    void *buff = TF_TensorData(OutputValues[0]);
    float *offsets = (float *)buff;
    return offsets[0];
}

int main() {

  srand(time(NULL));

  TF_Graph *Graph = TF_NewGraph();
  TF_Status *Status = TF_NewStatus();
  TF_SessionOptions *SessionOpts = TF_NewSessionOptions();
  TF_Buffer *RunOpts = NULL;
  TF_Library *library;

  const char *saved_model_dir = "FB_30_0_1_100_0.0_mape_all_350000_bbr_bw/";
  const char *tags = "serve";
  int ntags = 1;

  TF_Session *Session = TF_LoadSessionFromSavedModel(
      SessionOpts, RunOpts, saved_model_dir, &tags, ntags, Graph, NULL, Status);

   
    int NumInputs = 1;
    TF_Output* Input = malloc(sizeof(TF_Output) * NumInputs);
    TF_Output t0 = {TF_GraphOperationByName(Graph, "serving_default_input_1"), 0};
    if(t0.oper == NULL)
        printf("ERROR: Failed TF_GraphOperationByName serving_default_input_1\n");
    else
	printf("TF_GraphOperationByName serving_default_input_1 is OK\n");

    Input[0] = t0;

    int NumOutputs = 1;
    TF_Output* Output = malloc(sizeof(TF_Output) * NumOutputs);
    TF_Output t2 = {TF_GraphOperationByName(Graph, "StatefulPartitionedCall"), 0};
    if(t2.oper == NULL)
        printf("ERROR: Failed TF_GraphOperationByName StatefulPartitionedCall\n");
    else	
	printf("TF_GraphOperationByName StatefulPartitionedCall is OK\n");
    Output[0] = t2;

    TF_Tensor** InputValues = (TF_Tensor**)malloc(sizeof(TF_Tensor*)*NumInputs);
    TF_Tensor** OutputValues = (TF_Tensor**)malloc(sizeof(TF_Tensor*)*NumOutputs);

    float data[1][30][28];

    printf("Generate data\n");

    for (int i = 0; i < 30; i++)
    {
        for (int j = 0; j < 28; j++)
        {
            data[0][i][j] = rand();
            printf("%f ",data[0][i][j]);
        }
    }
    double res=predict(data, Input, InputValues, Output, OutputValues, Session);
    printf("Result: %f\n", res);
    return 0;
}