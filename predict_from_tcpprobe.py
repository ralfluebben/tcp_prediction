import numpy as np
import pandas as pd
import os

import gc
from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12
import matplotlib.pyplot as plt
import xarray as xr
import argparse
from keras_tuner import HyperParameters
from keras_tuner.tuners import Hyperband
from keras_tuner import HyperModel
import keras_tuner as kt
from keras_tuner.tuners import RandomSearch
from keras_tuner.tuners import BayesianOptimization
#from tensorflow.keras import mixed_precision
#policy = mixed_precision.Policy('mixed_float16')
#mixed_precision.set_global_policy(policy)
import tensorflow as tf
#os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
gpus = tf.config.list_physical_devices('GPU')
for g in gpus:
#  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_memory_growth(g, True)
  #   tf.config.experimental.set_virtual_device_configuration(
  #        gpus[0],
  #        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=3500)])
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

import math
from pathlib import Path
import windowgenerator

parser = argparse.ArgumentParser()
parser.add_argument("-t","--model_type", help="Model", type=str, required=True, choices=["Dense", "LSTM", "Feedback"])
parser.add_argument("-s","--input_start", help="Packet to start observation", type=int, required=True)
parser.add_argument("-w","--input_width", help="Trainlength used for prediction", type=int, required=True)
parser.add_argument("-f","--input_shift", help="Gap between input and output", type=int, required=True)
parser.add_argument("-l","--label_width", help="Output prediction length", type=int, required=True)
parser.add_argument("-r","--learning_rate", help="Learning rate to use: 0.0 meand adaptaive lr", type=float, required=True)
parser.add_argument("-m","--model_trial_id", help="Trained model use for plot timeseries", type=str)
parser.add_argument("-u","--tuner", help="Tuner to use", type=str, required=True, choices=["RandomSearch", "Hyperband", "BayesianOptimization"])
parser.add_argument("-b","--batch_size", help="Batch size", type=int, required=True)
parser.add_argument("-k","--keep", help="comma separated list of features to keep, all to keep all", type=str, required=True)
parser.add_argument("-n","--number_samples", help="number of samples", type=int, required=True)
args = parser.parse_args()

# load data
input_shift = args.input_shift
input_start = args.input_start
input_width = args.input_width
label_width = args.label_width
batch_size = args.batch_size
learning_rate_argv = args.learning_rate
modeltype = args.model_type
tuner = args.tuner
keep_label = args.keep
number_of_samples = args.number_samples
train_len = 300

np.random.seed(42)

#features_to_remove = ["sock_cookie"]

#df = xr.open_dataarray("data_small.nc")
#df1 = xr.open_dataarray("data.nc")[0:350000].astype(np.float32)
df1 = xr.open_dataarray("data32.nc")[0:number_of_samples].astype(np.float32)

print(df1.shape)
gc.collect()
feature_columns = list(df1.coords["values"].values)
num_features = len(feature_columns)
#label_columns=['tcpi_delivery_rate']
label_columns=['bbr_bw']
i_label = feature_columns.index(label_columns[0])
i_ts = feature_columns.index("ts")


# remove everything
#features_to_remove = feature_columns[:]
# do not remove label
#del features_to_remove[i_label]

#del features_to_remove[idx_keep_label]

#to_del = []
#for fea in features_to_remove:
#  i = feature_columns.index(fea)
#  to_del.append(i)

#i_to_del = sorted(set(to_del),reverse=True)

#print("Delete features")
#for index in i_to_del:
#    del feature_columns[index]

if keep_label == "all":
  test_label="all"
  keep_idxs = list(range(0,num_features))
  i_cookie = feature_columns.index("sock_cookie")
  del keep_idxs[i_cookie]
  idxs = sorted(set(keep_idxs),reverse=False)
else:
  keep_idxs = [ i_ts, i_label]
  for l in keep_label.split(","):
    if l.isdecimal():
      keep_idxs.append(int(l.strip()))
    else:
      i=feature_columns.index(l.strip())
      keep_idxs.append(i)
  idxs = sorted(set(keep_idxs),reverse=False)
  test_label=keep_label.replace(",","_")

df = df1[:,:,idxs]
del df1
gc.collect()
print(df.shape)
feature_columns = list(df.coords["values"].values)
i_label = feature_columns.index(label_columns[0])
num_features = len(feature_columns)
#label_columns=['BBR.BW']
print(feature_columns, len(feature_columns), i_label)

print(feature_columns)

n = len(df)
indices_train_1 = 0
indices_train_2 = int(n*0.8)
indices_val_1 = indices_train_2
indices_val_2 = int(n*0.9)
indices_test_1 = indices_val_2
indices_test_2 = n

train_df = df[indices_train_1:indices_train_2]
val_df = df[indices_val_1:indices_val_2]
test_df = df[indices_test_1:indices_test_2]
gc.collect()
del df
scaling=True
w1 = windowgenerator.WindowGenerator(input_start=input_start, input_width=input_width, label_width=label_width, shift=input_shift,  train_df=train_df, val_df=val_df, test_df=test_df, label_columns=label_columns, feature_columns=feature_columns, scaling=scaling)


#### LOSS Function

def tilted_loss(y_true, y_pred):
  q = q_alpha
  e = y_true - y_pred
  tl=tf.stack([q * e, (q - 1) * e])
  e_max=tf.math.reduce_max(tl,axis=0,keepdims=True)
  return tf.reduce_mean(e_max)

def mape_loss(y_true, y_pred):
  loss = tf.reduce_mean(tf.abs(tf.divide(tf.subtract(y_pred,y_true),(y_true))))
  if tf.math.is_inf(loss):
    tf.print(tf.subtract(y_pred,y_true))
    tf.print(y_true)
  return loss

#### NN Models: Dense, LSTM, Feedback, ...

class FeedBack(tf.keras.Model):
  def __init__(self, units, out_steps, dropout=0.0, _br=None, _rr=None, _kr=None):
    super().__init__()
    self.out_steps = out_steps
    self.units = units
    self.lstm_cell = tf.keras.layers.LSTMCell(units, 
      dropout=dropout,
      kernel_regularizer=_kr,
      recurrent_regularizer=_rr, 
      bias_regularizer=_br)
    # Also wrap the LSTMCell in an RNN to simplify the `warmup` method.
    self.lstm_rnn = tf.keras.layers.RNN(self.lstm_cell, return_state=True)
    self.dense = tf.keras.layers.Dense(num_features)

  def warmup(self, inputs):
    # inputs.shape => (batch, time, features)
    # x.shape => (batch, lstm_units)
    x, *state = self.lstm_rnn(inputs)

    # predictions.shape => (batch, features)
    prediction = self.dense(x)
    return prediction, state
  
  def call(self, inputs, training=None):
    # Use a TensorArray to capture dynamically unrolled outputs.
    predictions = []
    # Initialize the lstm state
    prediction, state = self.warmup(inputs)

    # Insert the first prediction
    predictions.append(prediction)

    # Run the rest of the prediction steps
    for n in range(1, self.out_steps):
      # Use the last prediction as input.
      x = prediction
      # Execute one lstm step.
      x, state = self.lstm_cell(x, states=state,
                                training=training)
      # Convert the lstm output to a prediction.
      prediction = self.dense(x)
      # Add the prediction to the output
      predictions.append(prediction)

    # predictions.shape => (time, batch, features)
    predictions = tf.stack(predictions)
    # predictions.shape => (batch, time, features)
    predictions = tf.transpose(predictions, [1, 0, 2])
    return predictions[:,:,i_label]

class FeedbackModel(HyperModel):
  def build(self, hp):
    print("### Build FeedbackModel model ###")
    _num_units_1 = hp.get('num_units_1')
    _learning_rate = hp.get('learning_rate')
    _optimizer = hp.get('optimizer')
    _dropout = hp.get('dropout')
    # regu
    _l2_kr_reg = hp.get('l2_kr_reg')
    _l2_rr_reg = hp.get('l2_rr_reg')
    _l2_reg = hp.get('l2_reg')
  
    _kr = None
    _br = None
    _rr = None

    if _l2_kr_reg > 0.0:
      _kr = tf.keras.regularizers.l2(l2=_l2_kr_reg)
    if _l2_rr_reg > 0.0:
      _rr = tf.keras.regularizers.l2(l2=_l2_rr_reg)
    if _l2_reg > 0.0:
      _br = tf.keras.regularizers.l2(l2=_l2_reg)


    model = FeedBack(_num_units_1, label_width, _dropout, _br, _rr, _kr)

    if _optimizer == 'adam':
      opt = tf.keras.optimizers.Adam(learning_rate= _learning_rate)
    
    model.compile(
        optimizer=opt,
        #loss=[tilted_loss],
        loss=[tf.keras.losses.MeanAbsolutePercentageError()],
        #loss=[mape_loss],
        metrics=[tf.keras.metrics.MeanAbsolutePercentageError()]
    )
    return model

class MyLSTMModel(HyperModel):
  def build(self, hp):
    print("### Build LSTM model ###")
    _num_units_1 = hp.get('num_units_1')
    _learning_rate = hp.get('learning_rate')
    _optimizer = hp.get('optimizer')

    model = tf.keras.Sequential([
      tf.keras.layers.LSTM(_num_units_1, return_sequences=False),
      tf.keras.layers.Dense(label_width)
    ])

    if _optimizer == 'adam':
      opt = tf.keras.optimizers.Adam(learning_rate= _learning_rate)

    #model.compile(
    #    optimizer=opt,
    #    loss=[tilted_loss],
    #    metrics=[metrics.QuantileMetric(), metrics.QuantileDistance(quantile=q_alpha, normalize=w1.get_label_norm())]
    #)

    model.compile(
        optimizer=opt,
        #loss=[tf.keras.losses.MeanSquaredError()],
        loss=[tf.keras.losses.MeanAbsolutePercentageError()],
        #metrics=[]
    )  


    return model

class MyDenseModel(HyperModel):
  def build(self, hp):
    _num_units_1 = hp.get('num_units_1')
    _num_units_2 = hp.get('num_units_2')
    _num_units_3 = hp.get('num_units_3')
    
    _learning_rate = hp.get('learning_rate')
    _dropout = hp.get('dropout')
    _l2_reg = hp.get('l2_reg')
    
    _optimizer = hp.get('optimizer')

    model = tf.keras.models.Sequential([
        tf.keras.layers.Masking(mask_value=0),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(_num_units_1, activation='relu',kernel_regularizer=tf.keras.regularizers.l2(_l2_reg)),
    ])

    # Add dropout only is greater 0.0
    if _dropout > 0.0:
        model.add(tf.keras.layers.Dropout(_dropout))

    # Add additional layer if > 0.0
    if _num_units_2 > 0:
        model.add(tf.keras.layers.Dense(_num_units_2, activation='relu',kernel_regularizer=tf.keras.regularizers.l2(_l2_reg)))
        if _num_units_3 > 0:
          model.add(tf.keras.layers.Dense(_num_units_3, activation='relu',kernel_regularizer=tf.keras.regularizers.l2(_l2_reg)))

    model.add(tf.keras.layers.Dense(label_width))

    if _optimizer == 'adam':
      opt = tf.keras.optimizers.Adam(learning_rate= _learning_rate)
    elif _optimizer == 'sgd':
      opt = tf.keras.optimizers.SGD(learning_rate= _learning_rate)
    elif _optimizer == 'rmsprop':
      opt = tf.keras.optimizers.RMSprop(learning_rate= _learning_rate)
    else:
      raise EnvironmentError("Unknown optimizer")

    model.compile(
        optimizer=opt,
        #loss=[tilted_loss],
        loss=[tf.keras.losses.MeanSquaredError()],
        #metrics=[metrics.QuantileMetric(), metrics.QuantileDistance(quantile=q_alpha, normalize=w1.get_label_norm())]
        metrics=[]
    )
    return model

########### Hyperparameters

q_alpha = 0.5
min_delta = 0.0001 # in ms, scaled later with 1/w1.std

q_alpha = "mape"
min_delta = 1 # in ms, scaled later with 1/w1.std

project_suffix = str(input_width) + "_" + str(input_start) +  "_" + str(label_width) +  "_" + str(input_shift) + "_" + str(learning_rate_argv) + "_" + str(q_alpha) + "_" + test_label + "_" + str(number_of_samples) + "_" + label_columns[0]

if scaling == False:
  project_suffix = project_suffix + "_noscaling"
else:
  project_suffix = project_suffix + "_scaling"

if modeltype == "Dense":
  hypermodel = MyDenseModel(tunable=False)
  project_name = "Dense" +  "_" + project_suffix
elif modeltype == "LSTM":
  hypermodel = MyLSTMModel(tunable=False)
  project_name = "LSTM" +  "_" + project_suffix
elif modeltype == "Feedback":
  hypermodel = FeedbackModel(tunable=False)
  project_name = "FB" +  "_" + project_suffix



if modeltype == "Dense":
  # "hp_learning_rate" : [0.0, 0.001, 0.0001, 0.00001]
  hps = { "hp_learning_rate" : [learning_rate_argv],
  "hp_num_units_1" : [30, 40, 100, 200, 500, 1200],
  "hp_num_units_2" : [0, 10, 20, 30, 40],
  "hp_num_units_3" : [0, 10, 20, 30],
  "hp_l2_reg" : [0.01, 0.001],
  "hp_optimizer" : ['adam'],
  "hp_dropout" : [0.0, 0.1],
  }
  epochs = 300
  use_testing_parameters = True
  # define testing parameters for fast evaluation
  if use_testing_parameters == False:
    hps = { "hp_learning_rate" : [0.0, 0.01],
    "hp_num_units_1" : [0, 1200],
    "hp_num_units_2" : [30],
    "hp_num_units_3" : [0],
    "hp_l2_reg" : [0.01],
    "hp_optimizer" : ['adam'],
    "hp_dropout" : [0.0]
    }
    epochs = 1000
elif modeltype == "LSTM":
  hps = { "hp_learning_rate" : [learning_rate_argv],
  "hp_num_units_1" : [5, 10, 20, 40, 70, 150, 300, 600],
  "hp_num_units_2" : [0],
  "hp_num_units_3" : [0],
  "hp_l2_reg" : [0.001],
  "hp_optimizer" : ['adam'],
  "hp_dropout" : [0.0],
  }
  epochs = 600
elif  modeltype == "Feedback":
  hps = { "hp_learning_rate" : [0.0, 0.001, 0.0001, 0.00001], #[learning_rate_argv],
  "hp_num_units_1" : [5, 10, 20, 40, 70, 150, 300, 600],
  "hp_num_units_2" : [0],
  "hp_num_units_3" : [0],
  "hp_l2_kr_reg" : [0.0, 0.01],
  "hp_l2_rr_reg" : [0.0, 0.01],
  "hp_l2_reg" : [0.0, 0.01],
  "hp_optimizer" : ['adam'],
  "hp_dropout" : [0.0, 0.2],
  "hp_batch_size" : [ 32, 128, 512, 4096],
  "hp_epochs" : [ 100, 300]
  }
  use_testing_parameters = False
  # define testing parameters for fast evaluation
  if use_testing_parameters == True:
    hps = { "hp_learning_rate" : [0.0], #[learning_rate_argv],
      "hp_num_units_1" : [70],
      "hp_num_units_2" : [0],
      "hp_num_units_3" : [0],
      "hp_l2_kr_reg" : [0.0],
      "hp_l2_rr_reg" : [0.0],
      "hp_l2_reg" : [0.01],
      "hp_optimizer" : ['adam'],
      "hp_dropout" : [0.0],
      "hp_batch_size" : [ 2048],
      "hp_epochs" : [ 300]
    }

# Calculate max trials for random search, for greedy approach
max_trials = 1
for key in hps:
  max_trials *= len(hps[key])
print("Max trials: " + str(max_trials))

max_trials = 50

hp = HyperParameters()
hp.Choice('learning_rate', values=hps["hp_learning_rate"])
hp.Choice('num_units_1', values=hps["hp_num_units_1"])
hp.Choice('num_units_2', values=hps["hp_num_units_2"])
hp.Choice('num_units_3', values=hps["hp_num_units_3"])
hp.Choice('l2_kr_reg', values=hps["hp_l2_kr_reg"])
hp.Choice('l2_rr_reg', values=hps["hp_l2_rr_reg"])
hp.Choice('l2_reg', values=hps["hp_l2_reg"])
hp.Choice('optimizer', values=hps["hp_optimizer"])
hp.Choice('dropout', values=hps["hp_dropout"])
hp.Choice('batch_size', values=hps["hp_batch_size"])
hp.Choice('epochs', values=hps["hp_epochs"])


############################

logdir = "results/" + project_name

def scheduler(epoch, lr):
  if epoch < epochs/4:
    return 0.01
  elif epoch < 2*epochs/4:
    return 0.001
  elif epoch < 3*epochs/4:
    return 0.0001
  else:
    return 0.00001

if tuner == "RandomSearch":
  class MyTuner(RandomSearch):
    def __init__(self,
                 hypermodel,
                 objective,
                 max_trials=max_trials,
                 seed=None,
                 hyperparameters=None,
                 tune_new_entries=True,
                 allow_new_entries=True,
                 **kwargs):
      super(MyTuner, self).__init__(
              hypermodel,
              objective,
              max_trials,
              seed,
              hyperparameters,
              tune_new_entries,
              allow_new_entries,
              **kwargs)

    def run_trial(self, trial, *fit_args, **fit_kwargs):
        global epochs
        values = trial.hyperparameters.get_config()['values']
        lr = values['learning_rate']
        fit_kwargs['batch_size'] = values['batch_size']
        epochs = fit_kwargs['epochs'] = values['epochs']
        if 'callbacks' not in fit_kwargs:
          fit_kwargs['callbacks'] = []
        if math.isclose(lr, 0.0, abs_tol=1e-6):
          lr_scheduler = tf.keras.callbacks.LearningRateScheduler(scheduler)
          print("ADD:" + str(type(lr_scheduler)))
          fit_kwargs['callbacks'].append(lr_scheduler)
          early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=int(epochs/4), min_delta=min_delta)
          print("ADD:" + str(type(early_stop)))
          fit_kwargs['callbacks'].append(early_stop)
        else: # add early stopping, if learning rate is fixed
          #early_stop = tf.keras.callbacks.EarlyStopping(monitor='quantile_distance', patience=10, min_delta=min_delta)
          early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, min_delta=min_delta)
          print("ADD:" + str(type(early_stop)))
          fit_kwargs['callbacks'].append(early_stop)
        
        # work around to skip invalid configurations 
        # if values['num_units_3'] > 0.1:
        #   if values['num_units_2'] < 0.1:
        #     terminate = tf.metrics.TerminateImmediatelyCallback()
        #     print("ADD:" + str(type(terminate)))
        #     fit_kwargs['callbacks'].append(terminate)
              
        super().run_trial(trial, *fit_args, **fit_kwargs)

        # remove callbacks added earlier, else they remain between runs
        to_be_removed = []
        for c in fit_kwargs['callbacks']:
          if isinstance(c, tf.keras.callbacks.EarlyStopping):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, tf.keras.callbacks.LearningRateScheduler):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
        for r in to_be_removed:
          fit_kwargs['callbacks'].remove(r)

    def on_trial_end(self, trial):
        global w1
        super().on_trial_end(trial)
        #values = trial.hyperparameters.get_config()['values']
        #model = self.load_model(trial)
        #model.load_weights(self._get_checkpoint_fname(trial.trial_id, self._reported_step))
        #loss, qm, qd = model.evaluate(w1.test[0], w1.test[1], batch_size=batch_size)
        #xhparams = {}
        #for p in myhparams:
        #  xhparams[p] = values[p.name] 
        # image_dir = logdir+ "/"+ trial.trial_id
        # with tf.summary.create_file_writer(image_dir).as_default():
        #   tf_hp.hparams(xhparams, trial_id=trial.trial_id)
        #   tf.summary.scalar("quantile_metric", qm, step=1)
        #   tf.summary.scalar("quantile_distance", qd, step=1)
        #   tf.summary.scalar("qtilted_loss", loss, step=1)
        #   w1.plot(model, run_dir=image_dir)
        #   w1.plot_mean(model, run_dir=image_dir)
        # model.save(image_dir + '/model.h5')
        #gc.collect()

  hptuner = MyTuner(
    hypermodel,
    hyperparameters=hp,
    objective=kt.Objective('val_loss', direction="min"),
    #objective=kerastuner.Objective('loss', direction="min"),
    max_trials=max_trials,
    directory='results',
    project_name=project_name,
    tune_new_entries=False,  # only use HPs listed before
    allow_new_entries=False
  )
if tuner == "Hyperband":
  class MyTuner(Hyperband):
    def __init__(self,
                 hypermodel,
                 objective,
                 max_epochs,
                 factor=3,
                 num_parameters=None,
                 hyperband_iteration=1,
                 seed=None,
                 hyperparameters=None,
                 tune_new_entries=True,
                 allow_new_entries=True,
                 **kwargs):
      super(MyTuner, self).__init__(
              hypermodel,
              objective,
              max_epochs,
              factor,
              num_parameters,
              hyperband_iteration,
              seed,
              hyperparameters,
              tune_new_entries,
              allow_new_entries,
              **kwargs)
    def run_trial(self, trial, *fit_args, **fit_kwargs):
        gc.collect()
        values = trial.hyperparameters.get_config()['values']
        lr = trial.hyperparameters.get_config()['values']['learning_rate']
        if 'callbacks' not in fit_kwargs:
          fit_kwargs['callbacks'] = []
        if math.isclose(lr, 0.0, abs_tol=1e-6):
          lr_scheduler = tf.keras.callbacks.LearningRateScheduler(scheduler)
          print("ADD:" + str(type(lr_scheduler)))
          fit_kwargs['callbacks'].append(lr_scheduler)
        else: # add early stopping, if learning rate is fixed
          early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, min_delta=min_delta)
          print("ADD:" + str(type(early_stop)))
          fit_kwargs['callbacks'].append(early_stop)
        
        # work around to skip invalid configurations 
        # if values['num_units_3'] > 0.1:
        #   if values['num_units_2'] < 0.1:
        #     terminate = tf.keras.callbacks.TerminateImmediatelyCallback()
        #     print("ADD:" + str(type(terminate)))
        #     fit_kwargs['callbacks'].append(terminate)
              
        super().run_trial(trial, *fit_args, **fit_kwargs)

        # remove callbacks added earlier, else they remain between runs
        to_be_removed = []
        for c in fit_kwargs['callbacks']:
          if isinstance(c, tf.keras.callbacks.EarlyStopping):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, tf.keras.callbacks.LearningRateScheduler):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
        for r in to_be_removed:
          fit_kwargs['callbacks'].remove(r)

  hptuner = MyTuner(
    hypermodel,
    num_parameters=9,
    hyperparameters=hp,
    max_epochs=epochs,
    objective='val_loss',
    directory='results',
    project_name=project_name,
    tune_new_entries=False,  # only use HPs listed before
    allow_new_entries=False
  )

if tuner == "BayesianOptimization":
  class MyTuner(BayesianOptimization):
    def __init__(self,
                 hypermodel,
                 objective,
                 max_trials,
                  num_initial_points=2,
                  alpha=0.0001,
                  beta=2.6,
                  seed=None,
                  hyperparameters=None,
                  tune_new_entries=True,
                  allow_new_entries=True,
                 **kwargs):
      super(MyTuner, self).__init__(
              hypermodel,
              objective,
              max_trials,
              num_initial_points,
              alpha,
              beta,
              seed,
              hyperparameters,
              tune_new_entries,
              allow_new_entries,
              **kwargs)

    def run_trial(self, trial, *fit_args, **fit_kwargs):
        #gc.collect()
        global epochs
        values = trial.hyperparameters.get_config()['values']
        lr = values['learning_rate']
        fit_kwargs['batch_size'] = values['batch_size']
        epochs = fit_kwargs['epochs'] = values['epochs']
        
        if 'callbacks' not in fit_kwargs:
          fit_kwargs['callbacks'] = []
        if math.isclose(lr, 0.0, abs_tol=1e-6):
          lr_scheduler = tf.keras.callbacks.LearningRateScheduler(scheduler)
          print("ADD:" + str(type(lr_scheduler)))
          fit_kwargs['callbacks'].append(lr_scheduler)
        else: # add early stopping, if learning rate is fixed
          #early_stop = tf.keras.callbacks.EarlyStopping(monitor='quantile_distance', patience=10, min_delta=min_delta)
          early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, min_delta=min_delta)
          print("ADD:" + str(type(early_stop)))
          fit_kwargs['callbacks'].append(early_stop)
        
        # work around to skip invalid configurations 
        # if values['num_units_3'] > 0.1:
        #   if values['num_units_2'] < 0.1:
        #     terminate = tf.metrics.TerminateImmediatelyCallback()
        #     print("ADD:" + str(type(terminate)))
        #     fit_kwargs['callbacks'].append(terminate)
              
        super().run_trial(trial, *fit_args, **fit_kwargs)

        # remove callbacks added earlier, else they remain between runs
        to_be_removed = []
        for c in fit_kwargs['callbacks']:
          if isinstance(c, tf.keras.callbacks.EarlyStopping):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
          elif isinstance(c, tf.keras.callbacks.LearningRateScheduler):
            print("REMOVE: "+ str(type(c)))
            to_be_removed.append(c)
        for r in to_be_removed:
          fit_kwargs['callbacks'].remove(r)

  hptuner = MyTuner(
    hypermodel,
    hyperparameters=hp,
    objective=kt.Objective('val_loss', direction="min"),
    max_trials=max_trials,
    directory='results',
    project_name=project_name,
    tune_new_entries=False,  # only use HPs listed before
    allow_new_entries=False
  )

if not args.model_trial_id:
  #train_dataset = w1.train #tf.data.Dataset.from_tensor_slices(w1.train).cache().batch(32*16)
  #val_dataset = w1.val #tf.data.Dataset.from_tensor_slices(w1.val).cache().batch(32*16)
  #hptuner.search(x=train_dataset,
  #  validation_data=val_dataset,
  #  use_multiprocessing=True
  #)
  x,y=w1.train
  x_val, y_val = w1.val
  hptuner.search(x,y,
    validation_data=(x_val,y_val),use_multiprocessing=True)
  model = hptuner.get_best_models(num_models=1)
  
  #w1.stats(model)
  print(type(model))
  print(type(w1.test[0]))
  print(type(w1.test[1]))
  #test_dataset = tf.data.Dataset.from_tensor_slices(w1.val).cache()
  model[0].evaluate(w1.test[0], w1.test[1], batch_size=512)
  model[0].summary()
  #w1.plot(model[0],fig_extension=project_name, plot_col=label_columns[0])
  #w1.plot_features()
  #w1.plot_mean(model[0],fig_extension=project_name)
  #w1.plot_single(model[0],fig_extension=project_name)
elif args.model_trial_id:
  plt.rcParams['text.usetex'] = True
  trial_fn = logdir + "/trial_" + args.model_trial_id +"/trial.json"
  print(trial_fn)
  trial = kt.engine.trial.Trial.load(trial_fn)
  model = hptuner.load_model(trial)
  #loss, qm, qd = model.evaluate(w1.test[0], w1.test[1], batch_size=batch_size)
  loss = model.evaluate(w1.test[0], w1.test[1], batch_size=512)
  print("Loss", loss)
  model.summary()
  model.save(project_name)

  #w1.stats(model)
  
  #w1.plot(model,fig_extension=project_name, plot_col=label_columns[0])
  
  #w1.plot_mean(model,fig_extension=project_name)
  #w1.plot_single(model,fig_extension=project_name, plot_col=label_columns[0])
  #w1.plot_ts_delay(fig_extension=project_name)
  #w1.plot_iat_delay(fig_extension=project_name)
  #w1.plot_hist(model, plot_col=label_columns[0])
  w1.plot_scatter(model,fig_extension=project_name, plot_col=label_columns[0])

  quit()
  i_label = feature_columns.index(label_columns[0])
  i_bbw_bw = feature_columns.index("bbr_bw")
  predictions = w1.get_prediction(model, plot_col=label_columns[0])
  t1=test_df[:,input_width:input_width+input_shift, 0].values
  r1=test_df[:,input_width:input_width+input_shift, i_label].values
  d1=np.sum(t1*r1, axis=1)/8
  d2=[]
  d3=[]
  d4=[]
  c1=0
  c2=0
  o=[]
  o2=[]

  for i in range(0,len(d1)):
    d2.append(np.sum(t1[i]*predictions[i]))
    if (predictions[i] > r1[i][-1]): # overestimation compared to label
      #print(i, predictions[i], r1[i][0], r1[i][-1], test_df[i,-1,20].values)
      c1+=1
      v=(predictions[i]-r1[i][-1])/r1[i][-1]
      o.append(v)
      d3.append(np.sum(t1[i]*r1[i][-1]))
      if v > 1:
        #print("1>10", i, predictions[i], test_df[i,-1,i_label].values, r1[i][-1], np.max(test_df[i,input_width+input_shift:-1,i_label].values))
        plt.close('all')
        plt.plot(test_df[i,:,i_label].values)
        plt.plot(test_df[i,:,i_bbw_bw].values)
        #plt.savefig("points_" + str(i)+ ".png")
        plt.close('all')
    else:
      o.append(0)
      d3.append(np.sum(t1[i]*predictions[i]))
    if (predictions[i] > test_df[i,-1,i_label].values): # overestimation compared to last sample 
      c2+=1
      v=(predictions[i]-test_df[i,-1,i_label].values)/test_df[i,-1,i_label].values
      o2.append(v)
      d4.append(np.sum(t1[i]*test_df[i,-1,i_label].values))
    else:
      o2.append(0)
      d4.append(np.sum(t1[i]*predictions[i]))
  d2=np.array(d2)/8
  d3=np.array(d3)/8
  d4=np.array(d4)/8

  r=(d2-d1)/d1
  r2=(d3-d1)/d1
  r3=(d4-d1)/d1
  #print(np.sum(r<0),np.sum(r>=0))
  #print(r)
  #print(c1/len(predictions))
  #print(c2/len(predictions))
  #print(o)
  #print(o2)
  plt.close('all')
  o=o*100
  bins=np.linspace(np.min(o),np.max(o), num=1000)
  #bins=np.linspace(-100,300, num=1000)
  #print(np.min(o),np.max(o))
  
  plt.hist(o, bins=bins, cumulative=-1, density=True, histtype='step', log=True)
  plt.grid()
  #plt.xlim((0,4))
  plt.xscale('log')
  plt.xlabel(r"overestimation factor $[\%]$")
  
  plt.ylabel(r"$P\left[ \frac{\hat{b}-b}{b} < x\right]$")
  plt.tight_layout()
  plt.savefig("hist_overestimate1.png")
  plt.close()
  o2=o2*100
  bins=np.linspace(np.min(o2),np.max(o2), num=1000)
  #print(np.min(o2),np.max(o2))
  
  plt.hist(o2, bins=1000, cumulative=-1, density=True, histtype='step', log=True)
  plt.grid()
  #plt.xlim((0,4))
  #plt.ylim((0.9,1))
  plt.xscale('log')
  plt.xlabel(r"overestimation factor $[\%]$")
  plt.ylabel(r"$P[ \frac{\hat{b}-b}{b} < x]$")
  plt.tight_layout()
  plt.savefig("hist_overestimate2.png")
  plt.close()
  plt.hist(r*100,  bins=1000, cumulative=True, density=True, histtype='step')
  #plt.hist(r2, bins=100, cumulative=True, density=True, histtype='step')
  #plt.hist(r3, bins=100, cumulative=True, density=True, histtype='step')
  plt.xlim((-100,300))
  plt.grid()
  plt.xlabel('over-/underestimation $[\%]$')
  plt.ylabel(r"$P[ \frac{\hat{v}-v}{v} < x]$")
  plt.tight_layout()
  plt.savefig("hist_estimate.png")

