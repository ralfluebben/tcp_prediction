import numpy as np
import pandas as pd
import windowgenerator
import os

import gc
from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.sans-serif'] = ['Times']
rcParams['lines.markersize'] = 4
rcParams['font.size'] = 12
import matplotlib.pyplot as plt
import xarray as xr
import argparse
from keras_tuner import HyperParameters
from keras_tuner.tuners import Hyperband
from keras_tuner import HyperModel
import keras_tuner as kt
from keras_tuner.tuners import RandomSearch
from keras_tuner.tuners import BayesianOptimization
#from tensorflow.keras import mixed_precision
#policy = mixed_precision.Policy('mixed_float16')
#mixed_precision.set_global_policy(policy)
import tensorflow as tf
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
gpus = tf.config.list_physical_devices('GPU')
for g in gpus:
#  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.experimental.set_memory_growth(g, True)
  #   tf.config.experimental.set_virtual_device_configuration(
  #        gpus[0],
  #        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=3500)])
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)


number_of_samples = 350000

df1 = xr.open_dataarray("data32.nc")[0:number_of_samples].astype(np.float32)

print(df1.shape)
gc.collect()
feature_columns = list(df1.coords["values"].values)
num_features = len(feature_columns)
#label_columns=['tcpi_delivery_rate']
label_columns=['bbr_bw']
i_label = feature_columns.index(label_columns[0])
i_ts = feature_columns.index("ts")

print(feature_columns)

# remove everything
#features_to_remove = feature_columns[:]
# do not remove label
#del features_to_remove[i_label]

#del features_to_remove[idx_keep_label]

#to_del = []
#for fea in features_to_remove:
#  i = feature_columns.index(fea)
#  to_del.append(i)

#i_to_del = sorted(set(to_del),reverse=True)

#print("Delete features")
#for index in i_to_del:
#    del feature_columns[index]

keep_label = "all"

if keep_label == "all":
  test_label="all"
  keep_idxs = list(range(0,num_features))
  i_cookie = feature_columns.index("sock_cookie")
  del keep_idxs[i_cookie]
  idxs = sorted(set(keep_idxs),reverse=False)
else:
  keep_idxs = [ i_ts, i_label]
  for l in keep_label.split(","):
    if l.isdecimal():
      keep_idxs.append(int(l.strip()))
    else:
      i=feature_columns.index(l.strip())
      keep_idxs.append(i)
  idxs = sorted(set(keep_idxs),reverse=False)
  test_label=keep_label.replace(",","_")

df = df1[:,:,idxs]
del df1
gc.collect()
print(df.shape)
feature_columns = list(df.coords["values"].values)
i_label = feature_columns.index(label_columns[0])
num_features = len(feature_columns)
#label_columns=['BBR.BW']
print(feature_columns, len(feature_columns), i_label)

print(feature_columns)

n = len(df)
indices_train_1 = 0
indices_train_2 = int(n*0.8)
indices_val_1 = indices_train_2
indices_val_2 = int(n*0.9)
indices_test_1 = indices_val_2
indices_test_2 = n

train_df = df[indices_train_1:indices_train_2]
val_df = df[indices_val_1:indices_val_2]
test_df = df[indices_test_1:indices_test_2]
gc.collect()
del df
scaling=True

input_shift = 100
input_start = 0
input_width = 30
label_width = 1

w1 = windowgenerator.WindowGenerator(input_start=input_start, input_width=input_width, label_width=label_width, shift=input_shift,  train_df=train_df, val_df=val_df, test_df=test_df, label_columns=label_columns, feature_columns=feature_columns, scaling=scaling)



df1 = xr.open_dataarray("data32.nc")[0:number_of_samples].astype(np.float32)

model = tf.saved_model.load("FB_30_0_1_100_0.0_mape_all_350000_bbr_bw/")
print(w1.test[0].shape)

input = w1.test[0][0].reshape((1,30,28))

print ("float data[0][30][28] { {", end="")
for i in range(30):
  print("{")
  for j in range(27):
    print (input[0][i][j], end=",")
  print (input[0][i][27], end="},")
print("} };")


print(input.shape)
#print(input)
#print(model.signatures)
f = model.signatures["serving_default"]
t = tf.constant(input)
import time
start = time.process_time()
print(w1.get_label_norm(), w1.get_label_mean())
res=f(input_1=t)['output_1'].numpy()[0][0]
print(res, res*w1.get_label_norm()-w1.get_label_mean())
#print(time.process_time() - start)